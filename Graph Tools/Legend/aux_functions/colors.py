import matplotlib as m

output = ""

output += "TABLEAU_COLORS"
output += "\n"
colors = m.colors.TABLEAU_COLORS
for color in colors:
    (r, g, b) = m.colors.to_rgb(color)
    output += '{:.0f};{:.0f};{:.0f}\n'.format(r * 255, g * 255, b * 255)

output += "\n"
output += "BASE_COLORS"
output += "\n"
colors = m.colors.BASE_COLORS
for color in colors:
    (r, g, b) = m.colors.to_rgb(color)
    output += '{:.0f};{:.0f};{:.0f}\n'.format(r * 255, g * 255, b * 255)

print(output)
